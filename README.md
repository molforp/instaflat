## Instagram client for [VK](https://vk.com/app4019543_7301483)

### Hosts
* production: [instaflat.parseapp.com](http://instaflat.parseapp.com/)
* stage (dev): [instaflat-dev.parseapp.com](http://instaflat-dev.parseapp.com/)

### Commands for deploy
`cd www`

* production: `parse deploy prod`
* stage (dev): `parse deploy dev`

App uses [Parse](https://www.parse.com) for backend

### PHP API for upload photos
`cd api` (git repo)

Use `git push` for deploy

App uses [openshift](https://www.openshift.com/) for php backend, [host](http://api-instaflat.rhcloud.com/)
