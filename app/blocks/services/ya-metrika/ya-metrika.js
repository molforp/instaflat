angular.module('services.metrika', [])
.run(function(metrikaService) {
    metrikaService.init();
})
.factory('metrikaService', function() {
    return {
        init: function() {
            /* jscs:disable */
            /* jshint ignore:start */
           (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter23198392 = new Ya.Metrika({id:23198392,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                ut:"noindex"});
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
            /* jshint ignore:end */
            /* jscs:enable */
        }
    };
});
