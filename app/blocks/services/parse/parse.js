angular.module('services.parse', ['app.config'])
.run(function(parseService) {
    parseService.init();
})
.factory('parseService', function(config) {
    return {
        init: function() {
            Parse.initialize(config.parseClientId, config.parseJsKey);
        }
    };
});
