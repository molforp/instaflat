angular.module('services.i18n', ['ngCookies', 'pascalprecht.translate'])
.config(function($translateProvider) {
    $translateProvider
        .translations('ru', {
            feed: 'Лента',
            popular: 'Популярное',
            likes: 'Лайки',
            users: 'Люди',
            self: 'Профиль',
            near: 'Рядом',
            search: 'Искать',
            enter: 'Войти',
            exit: 'Выйти',
            close: 'Закрыть',
            following: 'Вы подписаны',
            follow: 'Подписаться',
            photos: 'Фотографии',
            followers: 'Подписчики',
            follows: 'Подписки',
            byRequest: 'Запросили доступ',
            searchPhotoHint: 'Поиск фотографий',
            searchPeopleHint: 'Поиск пользователей по никнейму',
            notFound: 'По данному запросу ничего не найдено',
            setYourLocation: 'Нужно разрешить браузеру использовать ваше местоположение',
            privatUser: 'Пользователь установил настройки конфиденциальности',
            errorAlert: 'Произошла ошибка, попробуйте заново',
            errorLimit: 'Вы исчерпали свой лимит лайков. Разрешено 30 лайков в час',
            errorPhotoUpload: 'Загрузка отменена или не удалась',
            moreComments: 'Показать еще комментарии',
            likers: 'Оценили фотографию',
            showMap: 'Показать на карте',
            openOff: 'Открыть на instagram.com',
            inviteFriends: 'Пригласить друзей',
            saveWallPhoto: 'Сохранить фото себе на стену',
            ru: 'русский',
            en: 'english',
            viewPhotosInfo: 'Просматривайте свои и фото Ваших друзей',
            likesInfo: 'Ставьте лайки, читайте комментарии',
            popularInfo: 'Популярные фотографии, поиск людей, поиск по тэгам',
            allInfo: 'Все подробности&nbsp;',
            inOurGroup: 'в нашей группе ВКонтакте',
            seconds: 'сек',
            minutes: 'мин',
            hours: 'ч',
            days: 'д',
            weeks: 'нед',
            month: 'мес',
            years: 'г'
        })
        .translations('en', {
            feed: 'Feed',
            popular: 'Popular',
            likes: 'Likes',
            users: 'People',
            self: 'Profile',
            near: 'Near',
            search: 'Search',
            enter: 'Log in',
            exit: 'Exit',
            close: 'Close',
            following: 'Following',
            follow: 'Follow',
            photos: 'Posts',
            followers: 'Followers',
            follows: 'Following',
            byRequest: 'Requested permission',
            searchPhotoHint: 'Search photos',
            searchPeopleHint: 'Search people by name',
            setYourLocation: 'Allow the browser to use your location',
            notFound: 'Nothing found',
            privatUser: 'This user is privat',
            errorAlert: 'Error, try again',
            errorLimit: 'You have made 30 likes of the 30 allowed in the last hour',
            errorPhotoUpload: 'Failed or canceled',
            moreComments: 'Show more',
            likers: 'Likers',
            showMap: 'Show map',
            openOff: 'Open to instagram.com',
            inviteFriends: 'Invite friends',
            saveWallPhoto: 'Save the picture on the wall',
            ru: 'русский',
            en: 'english',
            viewPhotosInfo: 'Photos',
            likesInfo: 'Likes',
            popularInfo: 'Popular photos, people, tags',
            allInfo: 'All info',
            inOurGroup: 'in our VK community',
            seconds: 'sec',
            minutes: 'min',
            hours: 'h',
            days: 'd',
            weeks: 'w',
            month: 'm',
            years: 'y'
        })
        .determinePreferredLanguage()
        .registerAvailableLanguageKeys(['en', 'ru'], {
            'en_US': 'en',
            'en-US': 'en',
            'en_UK': 'en',
            'en-UK': 'en',
            'ru_RU': 'ru',
            'ru-RU': 'ru',
            'ru_UA': 'ru',
            'ru-UA': 'ru'
        })
        .fallbackLanguage('en')
        .useLocalStorage();
})
.factory('i18n', function($translate, $rootScope) {
    var dateTypes;

    function translateDate() {
        $translate(['years', 'month', 'weeks', 'days', 'hours', 'minutes', 'seconds']).then(function(types) {
            dateTypes = types;
        });
    }

    translateDate();

    $rootScope.$on('$translateChangeSuccess', function() {
        translateDate();
    });

    return {
        translate: $translate,
        use: $translate.use,
        langs: ['en', 'ru'],
        getDateTypes: function() {
            return dateTypes;
        }
    };
});
