angular.module('services.popup', ['services.instagram'])
.factory('popupService', function($document, $compile, $rootScope, instagramApiFactory) {
    var factory = {},
        popup = angular.element('<div insta-popup></div>'),
        paranja = angular.element('<div class="paranja" ng-click="closePopup()"></div>'),
        spinner = angular.element('<div class="spinner"></div>'),
        body = $document.find('body'),
        popupScope;

    function showPopup(scope, data) {
        scope.photo = data;
        // apply templates to popup element and link it to the scope
        $compile(popup)(scope);
        body.append(popup);
        spinner.remove();
    }

    factory.isOpened = false;
    factory.paranja = paranja;

    // open popup,
    factory.open = function(id, boxScope) {
        if (factory.isOpened) return;

        // create new scope
        popupScope = $rootScope.$new(true);

        body.append(paranja);
        body.append(spinner);
        $compile(paranja)(popupScope);
        popupScope.closePopup = factory.close;
        popupScope.boxScope = boxScope;

        instagramApiFactory.showPic(id).then(
            function(res) {
                factory.isOpened && showPopup(popupScope, res.data);
            },
            function(err) {
                console.log(err, 'Произошла ошибка');
            });

        factory.isOpened = true;
        body.addClass('page_scroll_no');
    };

    factory.close = function() {
        popup.remove();
        paranja.remove();
        spinner.remove();
        popupScope.$destroy();
        factory.isOpened = false;
        body.removeClass('page_scroll_no');
    };

    return factory;
});
