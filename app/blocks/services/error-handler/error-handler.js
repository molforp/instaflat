angular.module('services.errorHandler', ['ngProgressLite'])
.factory('errorHandler', function($q, $rootScope, $timeout, ngProgressLite) {
    return {
        responseError: function(err) {
            console.log(err);
            ngProgressLite.done();
            $rootScope.isError = true;
            $rootScope.errorAlert = 'errorAlert';

            $timeout(function() {
                $rootScope.isError = false;
            }, 4000);

            return $q.reject();
        }
    };
})
.config(function($httpProvider) {
    $httpProvider.interceptors.push('errorHandler');
});
