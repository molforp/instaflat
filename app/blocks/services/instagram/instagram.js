angular.module('services.instagram', ['ipCookie', 'app.config', 'ngProgressLite'])
.run(function(instagramAuthFactory) {
    instagramAuthFactory.init();
})
.factory('instagramAuthFactory', function($window, $interval, $state, ipCookie, config) {
    var AUTH_URL = 'https://api.instagram.com/oauth/authorize/?',
        CLIENT_ID = config.clientId,
        RET_PATH = config.retPath,
        LOGIN_STATE = 'login',
        scope = ['likes', 'comments', 'relationships'].join('+'),
        checkAuthInterval,
        authWindow,
        token;

    // get token from the hash and put it into the cookie or get it from the cookie
    function init() {
        token = getHashVal('access_token');

        if (token) {
            ipCookie('instaToken', token, { expires: 365 });
            return;
        }

        token = ipCookie('instaToken');
    }

    // login in instagram
    function login() {
        Parse.Analytics.track('login');

        var urlParams = [
            'client_id=' + CLIENT_ID,
            'redirect_uri=' + RET_PATH,
            'scope=' + scope,
            'response_type=token'
        ],
        url = AUTH_URL + urlParams.join('&');

        // open auth window
        authWindow = $window.open(url, 'InstagramAuth',
            'menubar=no, location=no, resizable=no, scrollbars=no ,status=no, width=600, height=400, top=200');

        // check if user is logged in
        checkAuthInterval = $interval(function() {
            checkAuth();
        }, 100);
    }

    // close auth window if user logged in
    function checkAuth() {
        (ipCookie('instaToken') && authWindow) && onAuth();
    }

    // выполняется когда юзер авторизовался и еще открыто окно авторизации
    function onAuth() {
        $interval.cancel(checkAuthInterval);
        authWindow.close();
        init();
        $state.go('feed', { type: 'feed' });
    }

    function isAuth() {
        return !!token;
    }

    // clear token on exit
    function logout() {
        Parse.Analytics.track('logout');

        token = null;
        ipCookie.remove('instaToken');
        $state.go(LOGIN_STATE);
    }

    function getToken() {
        return token;
    }

    // TODO: change?
    function getHashVal(key) {
        var substrNum,
            vars,
            varsLength,
            pair;

        if (location.hash) {
            substrNum = location.hash[1] === '/' ? 2 : 1,
            vars = location.hash.substring(substrNum).split('&'),
            varsLength = vars.length,
            pair;
        } else if (location.pathname) {
            vars = location.pathname.slice(1).split('&');
            varsLength = vars.length;
        }

        for (var i = 0; i < varsLength; i++) {
            pair = vars[i].split('=');
            if (pair[0] === key) return pair[1];
        }
    }

    return {
        CLIENT_ID: CLIENT_ID,
        init: init,
        getToken: getToken,
        isAuth: isAuth,
        login: login,
        logout: logout
    };
})
.factory('instagramApiFactory', function($q, $http, instagramAuthFactory) {
    var API_URL = 'https://api.instagram.com/v1/%endPoint%callback=JSON_CALLBACK';

    function request(endPoint, custParams) {
        var defer = $q.defer(),
            symbol = endPoint.indexOf('?') === -1 ? '?' : '&',
            url = API_URL.replace('%endPoint%', endPoint + symbol),
            sendParams = {
                access_token: instagramAuthFactory.getToken(),
                ts: _.now()
            };

        custParams && (sendParams = _.assign(sendParams, custParams));

        $http.jsonp(url, { params: sendParams })
            .success(function(res) {
                String(res.meta.code) === '200' ?
                    defer.resolve(res) :
                    defer.reject('Произошла ошибка. Попробуйте еще раз.');
            })
            .error(function() {
                defer.reject('Произошла ошибка. Попробуйте еще раз.');
            });

        return defer.promise;
    }

    // user's feed (my|popular etc)
    function getPics(val, custParams) {
        var endPoint = {
                my: 'users/self/media/recent',
                feed: 'users/self/feed',
                popular: 'media/popular',
                likes: 'users/self/media/liked',
                geo: '/media/search'
            }[val];

        return request(endPoint, custParams);
    }

    // some user photo
    function showPic(id) {
        return request('media/' + id);
    }

    // user info
    function getUser(id) {
        return request('users/' + id);
    }

    // user's photos
    function getUserPics(id, custParams) {
        return request('users/' + id + '/media/recent', custParams);
    }

    // some photo comments
    function getPicComments(id) {
        return request('media/' + id + '/comments');
    }

    // photo likes
    function getPicLikes(id) {
        return request('media/' + id + '/likes');
    }

    // tags
    function getTags(tag) {
        return request('tags/search?q=' + tag);
    }

    // search photos by the tag
    function getPicsByTag(tag, custParams) {
        Parse.Analytics.track('search', { type: 'tags' });
        return request('tags/' + tag + '/media/recent', custParams);
    }

    // check user status
    function isFollowed(id) {
        return request('users/' + id + '/relationship');
    }

    // user's follows
    function getFollows(id, custParams) {
        return request('users/' + id + '/follows', custParams);
    }

    // user's followers
    function getFollowers(id, custParams) {
        return request('users/' + id + '/followed-by', custParams);
    }

    function getRequested(id, custParams) {
        return request('users/self/requested-by', custParams);
    }

    // лайкнуть
    // function likePic(id, url) {
        // var url = url || 'http://' + BN('i-router').getHost() + '/api/user/like';
        // return this._request(url, null, { id: id });
    // }

    // like | unlike
    function likeUnlike(id, type) {
        return Parse.Cloud.run('likeUnlike', { id: id, token: instagramAuthFactory.getToken(), type: type });
    }

    // follow | unfollow
    function followUnfollow(id, action) {
        return Parse.Cloud.run('followUnfollow', { id: id, token: instagramAuthFactory.getToken(), action: action });
    }

    // search users by the name
    function getUsers(name) {
        Parse.Analytics.track('search', { type: 'users' });
        return request('users/search?q=' + name);
    }

    return {
        getPics: getPics,
        showPic: showPic,
        getUser: getUser,
        getUsers: getUsers,
        getUserPics: getUserPics,
        getPicComments: getPicComments,
        getPicLikes: getPicLikes,
        getTags: getTags,
        getPicsByTag: getPicsByTag,
        isFollowed: isFollowed,
        getFollows: getFollows,
        getFollowers: getFollowers,
        getRequested: getRequested,
        followUnfollow: followUnfollow,
        likeUnlike: likeUnlike
    };
}).factory('instagramUser', function($q, instagramApiFactory) {
    var id;

    function getId() {
        var defer = $q.defer();

        id ? defer.resolve(id) : instagramApiFactory.getUser('self').then(function(res) {
            id = res.data.id;
            defer.resolve(id);
        });

        return defer.promise;
    }

    return {
        getId: getId
    };
});
