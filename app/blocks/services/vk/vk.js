angular.module('services.vk', ['app.config'])
.run(function(vkService) {
    vkService.init();
})
.factory('vkService', function($window, config) {
    function init() {
        config.vk === 'prod' ? initProd() : initDev();

        setTimeout(function() {
            var adsParams = {'ad_unit_id':56093,'ad_unit_hash':'f977e42c464222cd14ba150ec53add11'};
            function vkAdsInit() {
                VK.Widgets.Ads('vk_ads_56093', {}, adsParams);
            }
            if (window.VK && VK.Widgets) {
                vkAdsInit();
            } else {
                if (!window.vkAsyncInitCallbacks) window.vkAsyncInitCallbacks = [];
                vkAsyncInitCallbacks.push(vkAdsInit);
                var protocol = ((location.protocol === 'https:') ? 'https:' : 'http:');
                var adsElem = document.getElementById('vk_ads_56093');
                var scriptElem = document.createElement('script');
                scriptElem.type = 'text/javascript';
                scriptElem.async = true;
                scriptElem.src = protocol + '//vk.com/js/api/openapi.js?116';
                adsElem.parentNode.insertBefore(scriptElem, adsElem.nextSibling);
            }
        }, 0);
    }

    function initDev() {
        window.vkAsyncInit = function() {
            VK.init({ apiId: config.vkId });
        };

        setTimeout(function() {
            var el = document.createElement('script');
            el.type = 'text/javascript';
            el.src = '//vk.com/js/api/openapi.js';
            el.async = true;
            document.getElementsByTagName('head')[0].appendChild(el);
        }, 0);
    }

    function initProd() {
        VK.init(function() {
            var query = location.search.slice(1).split('&');
            var queryParams = {};
            query.forEach(function(item) {
                var pair = item.split('=');
                queryParams[pair[0]] = pair[1]
            });
            var uid = queryParams.viewer_id;

            console.log('INSTAFLAT: VK init for viewer', uid);

            var user_id = uid || null;
            var app_id = 4019543;
            var a = new VKAdman();
            a.setupPreroll(app_id);
            a.onNoAds(function() { console.log("Adman: No ads"); });
            a.onStarted(function() { console.log("Adman: Started"); });
            a.onCompleted(function() { console.log("Adman: Completed"); });
            a.onSkipped(function() { console.log("Adman: Skipped"); });
            a.onClicked(function() { console.log("Adman: Clicked"); });
            admanStat(app_id, user_id);
        }, function() {
            console.log('vk init error!');
        }, '5.24');
    }

    return {
        init: init
    };
})
.factory('vkApiService', function($http, $q, config) {
    var API_PHP_URL = '//api-instaflat.rhcloud.com/upload_photo.php',
        APP_URL = 'https://vk.com/instaflat',
        randMessages = [
            'С помощью приложения',
            'Фото отправлено через приложение',
            'Через instagram клиент '
        ];

    function randomMsg(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function uploadWallPhoto(pid, photoUrl, uid, location, descr) {
        var defer = $q.defer();

        VK.api('photos.getWallUploadServer', {}, function(res) {
            (res.error || !res.response) && defer.reject();

            var params = {
                photo_id: pid,
                photo_url: photoUrl,
                upload_url: res.response.upload_url,
                callback: 'JSON_CALLBACK'
            };

            $http.jsonp(API_PHP_URL, { params: params })
                .success(function(res) {
                    if (!res.photo.length) return defer.reject();

                    var sendParams = res;
                    uid && (sendParams.user_id = uid);

                    VK.api('photos.saveWallPhoto', sendParams, function(res) {
                        if (res.error || !res.response) return defer.reject();

                        postWallPhoto(res.response[0], location, descr).then(
                            function(res) {
                                (res.error || !res.response) && defer.reject();

                                defer.resolve(res);
                            },
                            function() {
                                defer.reject();
                            });
                    });
                })
                .error(function() {
                    defer.reject();
                });
        });

        return defer.promise;
    }

    function postWallPhoto(params, location, descr) {
        var defer = $q.defer(),
            msgNum = randomMsg(0, 3),
            msgText = randMessages[msgNum] + ' ' + APP_URL + (descr ? '\n' + descr : ''),
            sendParams = {
                owner_id: params.owner_id,
                message: msgText,
                attachments: 'photo' + params.owner_id + '_' + (params.pid || params.id)
            };

        if (location) {
            sendParams.lat = location.latitude;
            sendParams.long = location.longitude;
        }

        VK.api('wall.post', sendParams, function(res) {
            (res.error || !res.response) ? defer.reject() : defer.resolve(res);
        });

        return defer.promise;
    }

    function scrollWindowTop() {
        config.vk === 'prod' ? VK.callMethod('scrollWindow', 0, 0) : '';
    }

    return {
        uploadWallPhoto: uploadWallPhoto,
        scrollWindowTop: scrollWindowTop
    };
});
