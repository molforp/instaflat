angular.module('components.popup', [
    'app.config',
    'components.likeBox',
    'components.map'
])
.directive('instaPopup', function($state, $stateParams, $document, $timeout, config,
    popupService, instagramApiFactory, vkApiService) {
    var paranja = popupService.paranja;

    return {
        replace: true,
        link: function(scope) {
            scope.close = function(uid) {
                if (uid && $stateParams.id === uid) return;
                popupService.close();
            };

            paranja.bind('click', scope.close);

            $document.bind('keypress', function(e) {
                var keyCode = e.which || e.keyCode;
                keyCode === 27 && scope.close();
            });

            setTimeout(function() {
                var adsParams = {'ad_unit_id':56111,'ad_unit_hash':'5bbbabbad2eb14ece3560f4f64e7f5ed'};
                function vkAdsInit() {
                  VK.Widgets.Ads('vk_ads_56111', {}, adsParams);
                }
                if (window.VK && VK.Widgets) {
                  vkAdsInit();
                } else {
                    if (!window.vkAsyncInitCallbacks) window.vkAsyncInitCallbacks = [];
                    vkAsyncInitCallbacks.push(vkAdsInit);
                    var protocol = ((location.protocol === 'https:') ? 'https:' : 'http:');
                    var adsElem = document.getElementById('vk_ads_56111');
                    var scriptElem = document.createElement('script');
                    scriptElem.type = 'text/javascript';
                    scriptElem.async = true;
                    scriptElem.src = protocol + '//vk.com/js/api/openapi.js?116';
                    adsElem.parentNode.insertBefore(scriptElem, adsElem.nextSibling);
                }
            }, 0);
        },
        controller: function($scope, $state) {
            var photo = $scope.photo,
                comments = photo.comments,
                map = photo.location;

            $scope.isImage = photo.type === 'image';
            $scope.isVideo = photo.type === 'video';
            $scope.isLiked = photo.user_has_liked;
            $scope.comments = comments.data;
            $scope.showMoreButton = comments.count > 8;

            $scope.dataMap = map && {
                name: map.name,
                center: [map.longitude, map.latitude],
                username: photo.user.full_name,
                userpic: photo.user.profile_picture,
                image: photo.images.low_resolution
            };

            $scope.likersIsOpen = false;
            $scope.mapIsOpen = false;
            $scope.photoUrl = photo.images.standard_resolution.url;

            instagramApiFactory.getPicLikes(photo.id).then(
                function(res) {
                    $scope.allLikes = res.data;
                    $scope.peopleLikes = res.data.slice(0, 7);
                }, function() {
                    $scope.peopleLikes = photo.likes && photo.likes.data;
                });

            $scope.showMoreComments = function() {
                Parse.Analytics.track('show_more_comments');

                instagramApiFactory.getPicComments(photo.id).then(function(res) {
                    $scope.showMoreButton = false;
                    $scope.comments = res.data;
                });
            };

            $scope.showLikers = function() {
                Parse.Analytics.track('show_likers');
                $scope.likersIsOpen = true;
            };

            $scope.hideLikers = function() {
                $scope.likersIsOpen = false;
            };

            $scope.toggleMap = function() {
                $scope.mapIsOpen = !$scope.mapIsOpen;
                $scope.mapIsOpen && Parse.Analytics.track('map');
            };

            $scope.hideMap = function() {
                $scope.mapIsOpen = false;
            };

            $scope.likeUnlike = function(id) {
                var type = $scope.isLiked ? 'unlike' : 'like';

                Parse.Analytics.track('like', { action: type, from: 'popup' });
                $scope.isLiked = !$scope.isLiked;
                instagramApiFactory.likeUnlike(id, type);
                $scope.boxScope.isLiked = $scope.isLiked;
            };

            $scope.findNear = function() {
                Parse.Analytics.track('popup_near_photo');
                $state.go('geo', { lat: map.latitude, lng: map.longitude });
            };

            $scope.savePhotoToWall = function(pid, photoUrl, uid) {
                var location = photo.location,
                    descr = photo.caption && photo.caption.text;

                Parse.Analytics.track('save_wall_photo', { type: 'click' });

                $scope.isPhotoUploading = true;

                vkApiService.uploadWallPhoto(pid, photoUrl, uid, location, descr).then(
                    function() {
                        Parse.Analytics.track('save_wall_photo', { type: 'done' });

                        $scope.isPhotoUploading = false;
                        $scope.isPhotoUploadDone = true;

                        $timeout(function() {
                            $scope.isPhotoUploadDone = false;
                        }, 1200);
                    },
                    function() {
                        Parse.Analytics.track('error', { type: 'wall_photo' });

                        $scope.isPhotoUploading = false;
                        $scope.isPhotoUploadFail = true;

                        $timeout(function() {
                            $scope.isPhotoUploadFail = false;
                        }, 2000);
                    });
            };
        },
        templateUrl: config.tplPath + '/popup.html',
    };
});
