module.exports.bemjson = {
    block: 'popup',
    content: [
        {
            elem: 'media',
            attrs: { bindonce: '' },
            content: [
                {
                    elem: 'photo',
                    tag: 'img',
                    mix: { block: 'image' },
                    attrs: { 'bo-if': 'isImage', 'bo-src': 'photo.images.standard_resolution.url' }
                },
                {
                    elem: 'video',
                    tag: 'video',
                    mix: { block: 'video' },
                    attrs: { 'bo-if': 'isVideo', controls: 'controls' },
                    content: {
                        block: 'video',
                        elem: 'source',
                        tag: 'source',
                        attrs: { 'bo-src': 'photo.videos.standard_resolution.url', type: 'video/mp4' }
                    }
                },
                {
                    block: 'popup',
                    elem: 'video-click',
                    attrs: { 'bo-if': 'isVideo' }
                },
                {
                    elem: 'like',
                    mix: [{ block: 'like' }, { block: 'icon', mods: { type: 'like' } }],
                    attrs: {
                        'ng-class': '{ like_liked_yes: isLiked }',
                        'ng-click': 'likeUnlike(photo.id)'
                    }
                },
                {
                    elem: 'save',
                    mix: { block: 'icon', mods: { type: 'save' } },
                    attrs: {
                        'bo-if': 'photo.type === "image"',
                        'ng-click': 'savePhotoToWall(photo.id, photoUrl)'
                    }
                },
                {
                    elem: 'wall-loader-wrap',
                    attrs: { 'ng-class': '{ "popup__wall-loader-wrap_visible": isPhotoUploading }' },
                    content: {
                        block: 'wall-spinner',
                        attrs: { 'ng-if': 'isPhotoUploading' },
                        content: [
                            { elem: 'bounce1' },
                            { elem: 'bounce2' },
                            { elem: 'bounce3' }
                        ]
                    }
                },
                {
                    elem: 'wall-loader-ok',
                    attrs: { 'ng-class': '{ "popup__wall-loader-ok_visible": isPhotoUploadDone }' },
                    content: {
                        block: 'icon',
                        mods: { type: 'ok' }
                    }
                },
                {
                    elem: 'wall-loader-fail',
                    attrs: { 'ng-class': '{ "popup__wall-loader-fail_visible": isPhotoUploadFail }' },
                    content: [
                        {
                            block: 'icon',
                            mods: { type: 'fail' }
                        },
                        {
                            elem: 'wall-loader-fail-text',
                            attrs: { translate: 'errorPhotoUpload' }
                        }
                    ]
                },
                {
                    elem: 'description',
                    attrs: {
                        'bo-if': 'photo.caption',
                        'bo-html': 'photo.caption.text | emoji',
                        'bo-class': '{ popup__description_video_yes: isVideo }'
                    }
                }
            ]
        },
        {
            elem: 'info',
            attrs: { bindonce: '' },
            content: [
                {
                    elem: 'top',
                    content: [
                        {
                            elem: 'popup-author-pic',
                            tag: 'a',
                            mix: { block: 'link' },
                            attrs: { 'ui-sref': 'user.feed({ id: photo.user.id })' },
                            content: {
                                elem: 'author-pic',
                                mix: { block: 'image' },
                                attrs: { style: 'background-image: url("{{ photo.user.profile_picture }}")' }
                            }
                        },
                        {
                            elem: 'user-info-inner',
                            content: [
                                {
                                    elem: 'user-info-top',
                                    mix: { block: 'cf' },
                                    content: [
                                        {
                                            elem: 'author-name',
                                            tag: 'a',
                                            mix: { block: 'link' },
                                            attrs: {
                                                'ui-sref': 'user.feed({ id: photo.user.id })',
                                                'bo-text': 'photo.user.username'
                                            }
                                        },
                                        {
                                            elem: 'date',
                                            content: {
                                                block: 'date-post',
                                                attrs: { 'bo-text': 'photo.created_time | myDate' }
                                            }
                                        },
                                        {
                                            elem: 'close',
                                            attrs: { 'ng-click': 'close()' }
                                        }
                                    ]
                                },
                                {
                                    elem: 'public-location',
                                    attrs: { 'bo-if': 'photo.location' },
                                    content: [
                                        {
                                            elem: 'photo-location',
                                            attrs: { 'ng-click': 'toggleMap()' },
                                            content: [
                                                {
                                                    block: 'icon',
                                                    mods: { type: 'location' }
                                                },
                                                {
                                                    block: 'popup',
                                                    elem: 'loc-text',
                                                    attrs: {
                                                        'bo-text': '(photo.location.name ||' +
                                                            ' "showMap" | translate)'
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            block: 'popup',
                                            elem: 'near-text',
                                            attrs: { translate: 'near', 'ng-click': 'findNear()' }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    elem: 'likes',
                    mix: { block: 'cf' },
                    content: [
                        {
                            elem: 'likebutton',
                            attrs: {
                                'ng-class': '{ popup__likebutton_liked_yes: isLiked }',
                                'ng-click': 'showLikers()'
                            },
                            content: [
                                {
                                    block: 'icon',
                                    mods: { type: 'like' }
                                },
                                {
                                    block: 'popup',
                                    elem: 'count-likes',
                                    attrs: { 'bo-text': 'photo.likes.count' }
                                }
                            ]
                        },
                        {
                            elem: 'piclike',
                            tag: 'a',
                            mix: [{ block: 'link' }, { block: 'hint' }],
                            attrs: {
                                'ng-repeat': 'pic in peopleLikes',
                                'ui-sref': 'user.feed({ id: pic.id })',
                                'data-name': '{{ pic.username }}',
                                style: 'background-image: url("{{ pic.profile_picture }}")'
                            }
                        }
                    ]
                },
                {
                    elem: 'open-off',
                    content: {
                        block: 'link',
                        tag: 'a',
                        attrs: { 'bo-href': 'photo.link', target: '_blank', translate: 'openOff' }
                    }
                },
                {
                    elem: 'location',
                    attrs: { 'ng-show': 'mapIsOpen' },
                    content: {
                        attrs: { 'insta-popup-map': '', info: 'dataMap', close: 'hideMap()' }
                    }
                },
                {
                    elem: 'comments',
                    content: {
                        elem: 'commentsinner',
                        attrs: { 'ng-class': '{ popup__commentsinner_more_hide: !showMoreButton }' },
                        content: [
                            {
                                attrs: { 'insta-popup-comment': '', 'ng-repeat': 'item in comments' }
                            },
                            {
                                elem: 'more-comments',
                                attrs: {
                                    'ng-if': 'showMoreButton',
                                    'translate': 'moreComments',
                                    'ng-click': 'showMoreComments()'
                                }
                            }
                        ]
                    }
                }
            ]
        },
        { attrs: { 'insta-like-box': '', users: 'allLikes', 'ng-if': 'likersIsOpen', 'close': 'hideLikers()' } },
        '<div id="vk_ads_56111"></div>',
        { block: 'like-box-paranja', attrs: { 'ng-if': 'likersIsOpen', 'ng-click': 'hideLikers()' } }
    ]
};
