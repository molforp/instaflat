angular.module('components.popup')
.directive('instaPopupComment', function(config) {
    return {
        replace: true,
        scope: {
            item: '='
        },
        templateUrl: config.tplPath + '/popup__item-comment.html'
    };
});
