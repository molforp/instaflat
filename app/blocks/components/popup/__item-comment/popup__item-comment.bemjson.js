module.exports.bemjson = {
    block: 'popup',
    elem: 'item-comment',
    attrs: { bindonce: '', item: 'item' },
    content: [
        {
            elem: 'comment-author',
            tag: 'a',
            mix: { block: 'link' },
            attrs: {
                'ui-sref': 'user.feed({ id: item.from.id })',
                'style': 'background-image: url({{ item.from.profile_picture }})'
            }
        },
        {
            elem: 'comment-info',
            content: [
                {
                    elem: 'comment-author-name',
                    tag: 'a',
                    mix: { block: 'link' },
                    attrs: {
                        'ui-sref': 'user.feed({ id: item.from.id })',
                        'bo-text': 'item.from.username'
                    }
                },
                {
                    elem: 'comment-date',
                    attrs: { 'bo-text': 'item.created_time | myDate' }
                },
                {
                    elem: 'comment-desc',
                    attrs: { 'bo-html': 'item.text | emoji' }
                }
            ]
        }
    ]
};
