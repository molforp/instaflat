angular.module('components.userList', ['services.instagram'])
.directive('instaUserList', function() {
    return {
        replace: true,
        scope: {
            users: '=',
            pager: '=',
            type: '@',
            uid: '@'
        },
        controller: function($scope, $timeout, instagramApiFactory) {
            var pager = $scope.pager || {},
                getUsersMethod = $scope.type === 'follows' ? 'getFollows' : 'getFollowers';

            $scope.nextCursor = pager.next_cursor;

            $scope.getItems = function(params) {
                $scope.isLoading = true;

                $timeout(function() {
                    instagramApiFactory[getUsersMethod]($scope.uid, params).then(
                        function(res) {
                            $scope.users = $scope.users.concat(res.data);
                            $scope.nextCursor = (res.pagination || {}).next_cursor;
                            $scope.isLoading = false;
                        },
                        function(err) {
                            console.log(err);
                            $scope.isLoading = false;
                        });
                });
            };

            $scope.$on('$stateChangeStart', function() {
                $scope.feedIsLoading = true;
            });

            $scope.$on('$stateChangeSuccess', function() {
                $scope.feedIsLoading = false;
            });
        },
        link: function(scope, element) {
            var offset = 700,
                elem = element[0];

            function onScroll() {
                if (!scope.nextCursor) return;

                if (!scope.isLoading && elem.scrollTop + elem.offsetHeight >= elem.scrollHeight - offset) {
                    scope.getItems({ cursor: scope.nextCursor });
                }
            }

            element.bind('scroll', _.throttle(onScroll, 250));
        },
        template: '<div class="user-list"><div class="user-list__inner">' +
            '<div class="error-text" ng-if="err" translate="{{ err }}"></div>' +
            '<div insta-user user="user" ng-repeat="user in users track by $index"></div>' +
        '<div class="user-list__fade" ng-class="{ \'user-list__fade_visible\': feedIsLoading }"></div></div></div>'
    };
});
