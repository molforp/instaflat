module.exports.bemjson = {
    block: 'user',
    attrs: { bindonce: '' },
    content: {
        elem: 'inner',
        content: [
            {
                elem: 'avatar',
                tag: 'a',
                mix: { block: 'link' },
                attrs: {
                    style: 'background-image: url("{{ user.profile_picture }}")',
                    'ui-sref': 'user.feed({ id: user.id })'
                }
            },
            {
                elem: 'info',
                content: {
                    elem: 'name',
                    tag: 'a',
                    mix: { block: 'link' },
                    attrs: { 'ui-sref': 'user.feed({ id: user.id })', 'bo-text': 'user.username' }
                }
            }
        ]
    }
};
