angular.module('components.user', ['app.config', 'pasvaz.bindonce'])
.directive('instaUser', function(config) {
    return {
        replace: true,
        scope: {
            user: '='
        },
        templateUrl: config.tplPath + '/user.html'
    };
});
