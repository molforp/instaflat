angular.module('components.langSwitcher', [])
.directive('instaLangSwitcher', function() {
    return {
        scope: {},
        template: '<div insta-select selected="selected" items="items" action="use(lang)"></div>',
        controller: function($scope, i18n) {
            var seletedLang,
                langs;

            i18n.translate(i18n.langs).then(function(translations) {
                seletedLang = i18n.use();

                $scope.selected = {
                    text: translations[seletedLang],
                    val: seletedLang
                };

                langs = translations;

                $scope.items = i18n.langs.map(function(item) {
                    return {
                        text: langs[item],
                        val: item
                    };
                });
            });

            $scope.use = i18n.use;
        }
    };
});
