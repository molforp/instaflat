angular.module('components.map', [
    'app.config',
    'yaMap'
])
.directive('instaPopupMap', function($compile, config) {
    return {
        replace: true,
        scope: {
            info: '=',
            close: '&'
        },
        controller: function($scope) {
            $scope.info && ($scope.point = {
                geometry: {
                    type:'Point',
                    coordinates: $scope.info.center
                }
            });
        },
        templateUrl: config.tplPath + '/map.html'
    };
});
