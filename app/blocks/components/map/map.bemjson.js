module.exports.bemjson = {
    block: 'map',
    content: [
        {
            elem: 'container',
            tag: 'ya-map',
            attrs: { 'ya-zoom': 12, 'ya-center': '{{ info.center }}', 'ya-controls': 'zoomControl' },
            content: [
                {
                    tag: 'ya-geo-object',
                    attrs: { 'ya-source': 'point' }
                }
            ]
        },
        {
            block: 'popup',
            elem: 'close-map',
            attrs: { 'ng-click': 'close()' },
            content: '✕'
        }
    ]
};
