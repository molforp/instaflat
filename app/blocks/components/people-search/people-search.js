angular.module('components.peopleSearch', [])
.directive('instaPeopleSearch', function($state) {
    return {
        replace: true,
        scope: {
            name: '@'
        },
        template: '<div bindonce class="people-search"><div class="people-search__control">' +
            '<input class="input people-search__input"' +
            'placeholder="{{ \'searchPeopleHint\' | translate }}" ng-model="name">' +
            '<div class="people-search__button" translate="search"></div></div></div>',
        link: function(scope, element) {
            var input = element.children().children().eq(0),
                button = element.children().children().eq(1);

            input.bind('keyup', _.throttle(function(e) {
                var keyCode = e.which || e.keyCode,
                    val = input.val();

                keyCode === 13 && val !== '' && $state.go('users', { name: encodeURIComponent(val) });
            }, 200));

            button.bind('click', function() {
                var val = input.val();

                $state.go('users', { name: encodeURIComponent(val) });
            });
        }
    };
});
