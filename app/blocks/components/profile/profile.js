angular.module('components.profile', [
    'app.config',
    'services.instagram'
])
.directive('instaProfile', function(config) {
    return {
        replace: true,
        scope: {
            user: '=',
        },
        controller: function($scope, $state, i18n, instagramAuthFactory, instagramApiFactory) {
            $scope.isFollow = $scope.user.isFollow;
            $scope.followTranslateKey = $scope.isFollow ? 'following' : 'follow';
            $scope.activeItem = $state.current.data.active;
            $scope.logout = instagramAuthFactory.logout;

            // $scope.user.isItMe && instagramApiFactory.getRequested().then(function(res) {
            //     !res.data.length && ($scope.requestedUsers = res.data);
            // });

            function translateButton() {
                i18n.translate($scope.followTranslateKey).then(function(text) {
                    $scope.followText = text;
                });
            }

            translateButton();

            $scope.followUnfollow = function(id) {
                var action = $scope.isFollow ? 'unfollow' : 'follow';

                instagramApiFactory.followUnfollow(id, action).then(function() {
                    $scope.isFollow = !$scope.isFollow;
                    $scope.followTranslateKey = $scope.isFollow ? 'following' : 'follow';
                    translateButton();
                });
            };

            $scope.$on('$stateChangeSuccess', function(e, toState) {
                $scope.activeItem = toState.data.active;
            });
        },
        templateUrl: config.tplPath + '/profile.html'
    };
});
