module.exports.bemjson = {
    block: 'profile',
    content: [
        {
            elem: 'user',
            attrs: { bindonce: '', 'ng-if': '!user.isPrivat' },
            content: [
                {
                    elem: 'avatar',
                    attrs: {  style: 'background-image: url("{{ user.profile_picture }}")' }
                },
                {
                    elem: 'info',
                    content: [
                        {
                            elem: 'name',
                            content: [
                                '{{ user.username }}',
                                {
                                    elem: 'open-off',
                                    mix: { block: 'link' },
                                    tag: 'a',
                                    attrs: {
                                        'bo-href': '"//instagram.com/" + user.username',
                                        target: '_blank',
                                        translate: 'openOff'
                                    }
                                }
                            ]
                        },
                        {
                            elem: 'bio',
                            content: [
                                {
                                    elem: 'full-name',
                                    attrs: { 'bo-if': 'user.full_name', 'bo-text': 'user.full_name + "."' }
                                },
                                {
                                    elem: 'description',
                                    attrs: { 'bo-if': 'user.bio', 'bo-text': 'user.bio' }
                                }
                            ]
                        },
                        {
                            elem: 'links',
                            content: {
                                block: 'link',
                                tag: 'a',
                                attrs: { target: '_blank', 'bo-href': 'user.website', 'bo-text': 'user.website' }
                            }
                        },
                        {
                            elem: 'button',
                            mix: { block: 'profile', elem: 'exit' },
                            mods: { type: 'exit' },
                            attrs: { 'bo-if': 'user.isItMe', translate: 'exit', 'ng-click': 'logout()' }
                        },
                        {
                            elem: 'button',
                            mix: { block: 'profile', elem: 'follow' },
                            mods: { type: 'follow' },
                            attrs: {
                                'ng-class': '{ profile__button_followed_yes: isFollow }',
                                'bo-if': '!user.isItMe',
                                'ng-click': 'followUnfollow(user.id)',
                                'ng-bind': 'followText'
                            }
                        }
                    ]
                }
            ]
        },
        {
            elem: 'stats',
            attrs: { bindonce: '', 'ng-if': '!user.isPrivat' },
            content: (function() {
                var items = [
                    {
                        count: 'user.counts.media',
                        sref: 'user.feed',
                        text: 'photos',
                        active: 'activeItem == "feed"'
                    },
                    {
                        count: 'user.counts.followed_by',
                        sref: 'user.followers',
                        text: 'followers',
                        active: 'activeItem == "followers"'
                    },
                    {
                        count: 'user.counts.follows',
                        sref: 'user.follows',
                        text: 'follows',
                        active: 'activeItem == "follows"'
                    }
                ];

                return items.map(function(item) {
                    return {
                        elem: 'stats-item',
                        attrs: { 'ng-class': '{ "profile__stats-item_active_yes": ' + item.active + ' }' },
                        content: [
                            {
                                elem: 'stats-count',
                                attrs: { 'bo-text': item.count }
                            },
                            {
                                elem: 'stats-text',
                                content: {
                                    block: 'link',
                                    tag: 'a',
                                    attrs: { 'ui-sref': item.sref, translate: item.text }
                                }
                            }
                        ]
                    };
                });
            })()
        },
        {
            block: 'error-text',
            mods: { type: 'profile' },
            attrs: { 'ng-if': 'user.isPrivat', translate: 'privatUser' }
        }
    ]
};
