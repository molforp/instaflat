module.exports.bemjson = {
    block: 'like-box',
    attrs: { bindonce: '' },
    content: [
        {
            elem: 'title',
            content: [
                {
                    elem: 'title-text',
                    attrs: { translate: 'likers' }
                },
                {
                    elem: 'close',
                    attrs: { translate: 'close', 'ng-click': 'close()' }
                }
            ]
        },
        {
            elem: 'inner',
            content: {
                elem: 'item',
                attrs: { 'ng-repeat': 'user in users' },
                content: {
                    elem: 'avatar',
                    tag: 'a',
                    mix: { block: 'link' },
                    attrs: {
                        'ui-sref': 'user.feed({ id: user.id })',
                        style: 'background-image: url("{{ user.profile_picture }}")'
                    },
                    content: {
                        block: 'like-box',
                        elem: 'username',
                        attrs: { 'bo-text': 'user.username' }
                    }
                }
            }
        }
    ]
};
