angular.module('components.likeBox', [
    'app.config',
    'services.instagram'
])
.directive('instaLikeBox', function(config) {
    return {
        replace: true,
        scope: {
            users: '=',
            close: '&'
        },
        templateUrl: config.tplPath + '/like-box.html'
    };
});
