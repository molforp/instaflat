angular.module('components.select', [])
.directive('instaSelect', function() {
    return {
        replace: true,
        scope: {
            items: '=',
            selected: '=',
            action: '&'
        },
        template: '<div class="select" ng-class="{ select_show_yes: isOpen }">' +
            '<div class="select__button" ng-bind="selected.text" ng-click="toggle()"></div>' +
            '<div class="select__content">' +
            '<div class="select__item" ng-repeat="item in items" ng-bind="item.text" ng-click="select(item)">' +
            '</div></div></div>',
        controller: function($scope) {
            $scope.isOpen = false;

            $scope.toggle = function() {
                $scope.isOpen = !$scope.isOpen;
            };

            $scope.select = function(item) {
                Parse.Analytics.track('switch_lang');

                $scope.selected = {
                    text: item.text,
                    val: item.val
                };

                $scope.action({ lang: item.val });

                $scope.isOpen = false;
            };

            $scope.close = function() {
                $scope.isOpen = false;
            };
        }
    };
});
