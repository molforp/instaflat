angular.module('components.headerSearch', [])
.directive('instaHeaderSearch', function($state) {
    return {
        replace: true,
        template: '<input class="input input_type_header"' +
            'placeholder="{{ \'searchPhotoHint\' | translate }}" ng-model="tag">',
        link: function(scope, element) {
            scope.$watch('tag', function(val) {
                val && (scope.tag = val.replace(/[^\wА-Яа-я]/g, ''));
            });

            element.bind('keyup', _.throttle(function(e) {
                var keyCode = e.which || e.keyCode;
                keyCode === 13 && scope.tag !== '' && $state.go('tags', { tag: encodeURIComponent(scope.tag) });
            }, 200));

            element.bind('blur', function() {
                element.val(element.val().replace(/\s/g, ''));
            });
        }
    };
});
