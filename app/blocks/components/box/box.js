angular.module('components.box', [
    'app.config',
    'services.instagram',
    'services.popup'
])
.directive('instaBox', function($rootScope, $compile, $timeout, config) {
    return {
        replace: true,
        scope: {
            box: '='
        },
        controller: function($scope, $timeout, popupService, instagramApiFactory, vkApiService) {
            $scope.isLiked = $scope.box.user_has_liked;
            $scope.photoUrl = $scope.box.images.standard_resolution.url;

            $scope.openPopup = function(id) {
                vkApiService.scrollWindowTop();
                popupService.open(id, $scope);
            };

            $scope.likeUnlike = function(id) {
                var type = $scope.isLiked ? 'unlike' : 'like';

                Parse.Analytics.track('like', { action: type, from: 'feed' });
                $scope.isLiked = !$scope.isLiked;
                instagramApiFactory.likeUnlike(id, type).fail(function(err) {
                    if (err.message === 'OAuthRateLimitException') {
                        Parse.Analytics.track('error', { type: 'likes_limit' });
                        $rootScope.isError = true;
                        $rootScope.errorAlert = 'errorLimit';
                        $scope.isLiked = false;

                        $timeout(function() {
                            $rootScope.isError = false;
                        }, 4000);
                    }
                });
            };

            $scope.savePhotoToWall = function(pid, photoUrl, uid) {
                var location = $scope.box.location,
                    descr = $scope.box.caption && $scope.box.caption.text;

                Parse.Analytics.track('save_wall_photo', { type: 'click' });

                $scope.isPhotoUploading = true;

                vkApiService.uploadWallPhoto(pid, photoUrl, uid, location, descr).then(
                    function() {
                        Parse.Analytics.track('save_wall_photo', { type: 'done' });

                        $scope.isPhotoUploading = false;
                        $scope.isPhotoUploadDone = true;

                        $timeout(function() {
                            $scope.isPhotoUploadDone = false;
                        }, 1200);
                    },
                    function() {
                        Parse.Analytics.track('error', { type: 'wall_photo' });

                        $scope.isPhotoUploading = false;
                        $scope.isPhotoUploadFail = true;

                        $timeout(function() {
                            $scope.isPhotoUploadFail = false;
                        }, 2000);
                    });
            };
        },
        templateUrl: config.tplPath + '/box.html'
    };
});
