module.exports.bemjson = {
    block: 'box',
    attrs: { bindonce: '', block: 'box', class: 'box_type_{{ box.type }}' },
    content: [
        {
            elem: 'photo',
            tag: 'img',
            mix: { block: 'image' },
            attrs: { 'bo-src': 'box.images.low_resolution.url', 'ng-click': 'openPopup(box.id)' }
        },
        {
            elem: 'video',
            attrs: { 'bo-if': 'box.type === "video"' },
            content: {
                block: 'icon',
                mods: { type: 'video' },
                attrs: { 'ng-click': 'openPopup(box.id)' }
            }
        },
        {
            elem: 'like',
            mix: [{ block: 'like' }, { block: 'icon', mods: { type: 'like' } }],
            attrs: {
                'ng-class': '{ like_liked_yes: isLiked }',
                'ng-click': 'likeUnlike(box.id)'
            }
        },
        {
            elem: 'save',
            mix: { block: 'icon', mods: { type: 'save' } },
            attrs: {
                'bo-if': 'box.type === "image"',
                'ng-click': 'savePhotoToWall(box.id, photoUrl)'
            }
        },
        {
            elem: 'wall-loader-wrap',
            attrs: { 'ng-class': '{ "box__wall-loader-wrap_visible": isPhotoUploading }' },
            content: {
                block: 'wall-spinner',
                attrs: { 'ng-if': 'isPhotoUploading' },
                content: [
                    { elem: 'bounce1' },
                    { elem: 'bounce2' },
                    { elem: 'bounce3' }
                ]
            }
        },
        {
            elem: 'wall-loader-ok',
            attrs: { 'ng-class': '{ "box__wall-loader-ok_visible": isPhotoUploadDone }' },
            content: {
                block: 'icon',
                mods: { type: 'ok' }
            }
        },
        {
            elem: 'wall-loader-fail',
            attrs: { 'ng-class': '{ "box__wall-loader-fail_visible": isPhotoUploadFail }' },
            content: [
                {
                    block: 'icon',
                    mods: { type: 'fail' }
                },
                {
                    elem: 'wall-loader-fail-text',
                    attrs: { translate: 'errorPhotoUpload' }
                }
            ]
        },
        {
            elem: 'bottom-panel',
            content: {
                elem: 'user-info',
                content: [
                    {
                        elem: 'user-pic',
                        tag: 'a',
                        mix: [{ block: 'link' }, { block: 'userpic' }],
                        attrs: {
                            'ui-sref': 'user.feed({ id: box.user.id })',
                            'style': 'background-image: url("{{ box.user.profile_picture }}")'
                        }
                    },
                    {
                        elem: 'user-activity',
                        content: [
                            {
                                elem: 'user-name',
                                content: {
                                    block: 'link',
                                    tag: 'a',
                                    attrs: {
                                        'bo-text': 'box.user.username',
                                        'ui-sref': 'user.feed({ id: box.user.id })'
                                    }
                                }
                            },
                            {
                                elem: 'likes',
                                content: [
                                    {
                                        elem: 'likeicon',
                                        mix: { block: 'icon', mods: { type: 'like' } },
                                        attrs: { 'ng-class': '{ box__likeicon_liked_yes: isLiked }' }
                                    },
                                    {
                                        elem: 'like-count',
                                        attrs: { 'bo-text': 'box.likes.count' }
                                    }
                                ]
                            },
                            {
                                elem: 'comments',
                                content: [
                                    {
                                        block: 'icon',
                                        mods: { type: 'comment' }
                                    },
                                    {
                                        elem: 'comment-count',
                                        attrs: { 'bo-text': 'box.comments.count' }
                                    }
                                ]
                            },
                            {
                                elem: 'geo',
                                attrs: { 'bo-if': 'box.location' },
                                content: {
                                    block: 'icon',
                                    mods: { type: 'location' }
                                }
                            },
                            {
                                elem: 'date',
                                content: [
                                    {
                                        block: 'icon',
                                        mods: { type: 'date' }
                                    },
                                    {
                                        elem: 'date-post',
                                        attrs: { 'bo-text': 'box.created_time | myDate' }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        }
    ]
};
