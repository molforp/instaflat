angular.module('components.login', [
    'app.config',
    'services.instagram'
])
.directive('instaLogin', function(config, $document) {
    return {
        replace: true,
        controller: function($scope, instagramAuthFactory) {
            var body = $document.find('body');

            body.addClass('page_login');
            $scope.login = instagramAuthFactory.login;

            $scope.$on('$destroy', function() {
                body.removeClass('page_login');
            });
        },
        templateUrl: config.tplPath + '/login.html'
    };
});
