module.exports.bemjson = {
    block: 'login',
    content: {
        elem: 'inner',
        content: [
            { elem: 'icon' },
            {
                elem: 'content',
                content: [
                    { elem: 'logo' },
                    {
                        block: 'lang-switcher',
                        mods: { type: 'login' },
                        attrs: { 'insta-lang-switcher': '' }
                    },
                    {
                        block: 'list',
                        mix: { block: 'login', elem: 'list' },
                        content: [
                            {
                                elem: 'item',
                                attrs: { translate: 'viewPhotosInfo' }
                            },
                            {
                                elem: 'item',
                                attrs: { translate: 'likesInfo' }
                            },
                            {
                                elem: 'item',
                                attrs: { translate: 'popularInfo' }
                            },
                            {
                                elem: 'item',
                                content: [
                                    {
                                        tag: 'span',
                                        attrs: { translate: 'allInfo' }
                                    },
                                    '&nbsp;',
                                    {
                                        block: 'link',
                                        tag: 'a',
                                        attrs: {
                                            href: 'http://vk.com/instappvk',
                                            target: '_blank',
                                            translate: 'inOurGroup'
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        elem: 'button',
                        attrs: { 'ng-click': 'login()', translate: 'enter' }
                    }
                ]
            }
        ]
    }
};
