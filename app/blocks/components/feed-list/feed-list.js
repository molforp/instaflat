angular.module('components.feedList', ['services.instagram'])
.directive('instaFeedList', function() {
    return {
        replace: true,
        scope: {
            items: '=',
            pager: '=',
            type: '@',
            uid: '@',
            tag: '@',
            geo: '=',
            err: '@'
        },
        controller: function($scope, $timeout, instagramApiFactory) {
            var pager = $scope.pager || {},
                endPoint = $scope.type || $scope.uid || $scope.tag,
                getPicsMethod = $scope.type ? 'getPics' :
                    $scope.tag ? 'getPicsByTag' : 'getUserPics';

            $scope.isPopularOrGeo = $scope.type === 'popular' || $scope.type === 'geo';

            var params = ['like', 'tag', ''];

            // different endpoints has differrent max_id params
            params.some(function(type) {
                var param = type ? 'next_max_' + type + '_id' : 'next_max_id';
                $scope.nextMaxId = pager[param];
                $scope.maxIdParam = param.replace('next_', '');
                return pager[param];
            });

            $scope.getItems = function(params) {
                $scope.isLoading = true;

                $timeout(function() {
                    instagramApiFactory[getPicsMethod](endPoint, params).then(
                        function(res) {
                            var pager = res.pagination || {},
                                data = res.data;

                            $scope.items = $scope.items.concat(data);
                            $scope.nextMaxId = pager.next_max_id || pager.next_max_like_id;

                            if ($scope.geo) {
                                $scope.geo.max_timestamp = _.parseInt(data[res.data.length - 1].created_time) - 1000;
                            }

                            $scope.isLoading = false;
                        },
                        function(err) {
                            console.log(err);
                            $scope.isLoading = false;
                        });
                });
            };

            $scope.$on('$stateChangeStart', function() {
                $scope.feedIsLoading = true;
            });

            $scope.$on('$stateChangeSuccess', function() {
                $scope.feedIsLoading = false;
            });
        },
        link: function(scope, element) {
            var offset = 700,
                elem = element[0],
                reqParams = scope.geo || {};

            function onScroll() {
                if (!scope.nextMaxId && !scope.isPopularOrGeo) return;

                if (!scope.isLoading && elem.scrollTop + elem.offsetHeight >= elem.scrollHeight - offset) {
                    scope.isPopularOrGeo || (reqParams[scope.maxIdParam] = scope.nextMaxId);
                    scope.getItems(reqParams);
                }
            }

            element.bind('scroll', _.throttle(onScroll, 250));
        },
        template: '<div class="feed-list">' +
            '<div class="feed-list__inner">' +
            '<div class="error-text" ng-if="err" translate="{{ err }}"></div>' +
            '<div insta-box box="item" ng-repeat="item in items track by $index"></div>' +
        '<div class="feed-list__fade" ng-class="{ \'feed-list__fade_visible\': feedIsLoading }"></div></div></div>'
    };
});
