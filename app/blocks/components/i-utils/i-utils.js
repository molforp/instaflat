angular.module('components.i-utils', [])
.filter('myDate', function(i18n) {
    return function(time) {
        var round = Math.round,
            date = Math.abs(time * 1000),
            seconds = round((new Date() - date) / 1000),
            minutes = round(seconds / 60),
            hours = round(minutes / 60),
            days = round(hours / 24),
            weeks = round(days / 7),
            month = round(days / 30),
            years = round(days / 365),
            dateTypes = i18n.getDateTypes();

        function getTime() {
            if (years > 1) {
                return years + ' ' + dateTypes.years;
            }
            if (month > 1) {
                return month + ' ' + dateTypes.month;
            }
            if (weeks > 1) {
                return weeks + ' ' + dateTypes.weeks;
            }
            if (days > 1) {
                return days + ' ' + dateTypes.days;
            }
            if (hours > 1) {
                return hours + ' ' + dateTypes.hours;
            }
            if (minutes > 1) {
                return minutes + ' ' + dateTypes.minutes;
            }
            if (seconds > 1) {
                return seconds + ' ' + dateTypes.seconds;
            }
        }

        return getTime();
    };
})
.filter('emoji', function() {
    return function(text) {
        return text && text
            .replace(/😊/g,'<img src="//vk.com/images/emoji/D83DDE0A.png">')
            .replace(/😃/g,'<img src="//vk.com/images/emoji/D83DDE03.png">')
            .replace(/😆/g,'<img src="//vk.com/images/emoji/D83DDE06.png">')
            .replace(/😉/g,'<img src="//vk.com/images/emoji/D83DDE09.png">')
            .replace(/😜/g,'<img src="//vk.com/images/emoji/D83DDE1C.png">')
            .replace(/😋/g,'<img src="//vk.com/images/emoji/D83DDE0B.png">')
            .replace(/😍/g,'<img src="//vk.com/images/emoji/D83DDE0D.png">')
            .replace(/😎/g,'<img src="//vk.com/images/emoji/D83DDE0E.png">')
            .replace(/😒/g,'<img src="//vk.com/images/emoji/D83DDE12.png">')
            .replace(/😏/g,'<img src="//vk.com/images/emoji/D83DDE0F.png">')
            .replace(/😔/g,'<img src="//vk.com/images/emoji/D83DDE14.png">')
            .replace(/😢/g,'<img src="//vk.com/images/emoji/D83DDE22.png">')
            .replace(/😭/g,'<img src="//vk.com/images/emoji/D83DDE2D.png">')
            .replace(/😩/g,'<img src="//vk.com/images/emoji/D83DDE29.png">')
            .replace(/😨/g,'<img src="//vk.com/images/emoji/D83DDE28.png">')
            .replace(/😐/g,'<img src="//vk.com/images/emoji/D83DDE10.png">')
            .replace(/😌/g,'<img src="//vk.com/images/emoji/D83DDE0C.png">')
            .replace(/😠/g,'<img src="//vk.com/images/emoji/D83DDE20.png">')
            .replace(/😡/g,'<img src="//vk.com/images/emoji/D83DDE21.png">')
            .replace(/😇/g,'<img src="//vk.com/images/emoji/D83DDE07.png">')
            .replace(/😰/g,'<img src="//vk.com/images/emoji/D83DDE30.png">')
            .replace(/😲/g,'<img src="//vk.com/images/emoji/D83DDE32.png">')
            .replace(/😳/g,'<img src="//vk.com/images/emoji/D83DDE33.png">')
            .replace(/😷/g,'<img src="//vk.com/images/emoji/D83DDE37.png">')
            .replace(/😚/g,'<img src="//vk.com/images/emoji/D83DDE1A.png">')
            .replace(/😈/g,'<img src="//vk.com/images/emoji/D83DDE08.png">')
            .replace(/❤/g,'<img src="//vk.com/images/emoji/2764.png">')
            .replace(/👍/g,'<img src="//vk.com/images/emoji/D83DDC4D.png">')
            .replace(/👎/g,'<img src="//vk.com/images/emoji/D83DDC4E.png">')
            .replace(/☝/g,'<img src="//vk.com/images/emoji/261D.png">')
            .replace(/✌/g,'<img src="//vk.com/images/emoji/270C.png">')
            .replace(/👌/g,'<img src="//vk.com/images/emoji/D83DDC4C.png">')
            .replace(/✨/g,'<img src="//vk.com/images/emoji/D83CDF1F.png">')
            .replace(/👊/g,'<img src="//vk.com/images/emoji/D83DDC4A.png">')
            .replace(/👏/g,'<img src="//vk.com/images/emoji/D83DDC4F.png">')
            .replace(/💙/g,'<img src="//vk.com/images/emoji/D83DDC99.png">')
            .replace(/💙/g,'<img src="//vk.com/images/emoji/D83DDC99.png">');
    };
});
