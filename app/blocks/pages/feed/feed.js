angular.module('pages.feed', ['ui.router'])
.config(function($stateProvider) {
    $stateProvider
        .state('feed', {
            url: '/feed/:type',
            resolve: {
                feed: function($q, $stateParams, $timeout, $state, instagramApiFactory) {
                    if (!_.contains(['feed', 'my', 'popular', 'likes'], $stateParams.type)) {
                        $state.go('feed', { type: 'feed' });
                    }

                    var defer = $q.defer();

                    $timeout(function() {
                        instagramApiFactory.getPics($stateParams.type).then(
                            function(res) {
                                if (!res.data.length) {
                                    defer.resolve({ err: 'notFound' });
                                    return;
                                }

                                defer.resolve(res);
                            },
                            function(err) {
                                defer.reject(err);
                            });
                    });

                    return defer.promise;
                }
            },
            template: '<div insta-feed-list items="items" pager="pager" type="{{ type }}" err="{{ err }}"></div>',
            controller: 'feedCtrl',
            data: { pageTitle: 'Лента' }
        })
        .state('tags', {
            url: '/tags/:tag',
            resolve: {
                feed: function($q, $stateParams, $timeout, $state, instagramApiFactory) {
                    !$stateParams.tag && $state.go('feed', { type: 'feed' });

                    var defer = $q.defer();

                    $timeout(function() {
                        instagramApiFactory.getPicsByTag($stateParams.tag).then(
                            function(res) {
                                if (!res.data.length) {
                                    defer.resolve({ err: 'notFound' });
                                    return;
                                }

                                defer.resolve(res);
                            },
                            function(err) {
                                defer.reject(err);
                            });
                    });

                    return defer.promise;
                }
            },
            template: '<div insta-feed-list items="items" pager="pager" tag="{{ tag }}" err="{{ err }}"></div>',
            controller: 'feedCtrl',
            data: { pageTitle: 'Поиск по тегам' }
        })
        .state('geo', {
            url: '/geo?type&lat&lng&dist',
            resolve: {
                feed: function($q, $stateParams, $timeout, $state, instagramApiFactory, geolocation) {
                    var defer = $q.defer();

                    function getPics(params) {
                        $timeout(function() {
                            instagramApiFactory.getPics('geo', params).then(
                                function(res) {
                                    if (!res.data.length) {
                                        defer.resolve({ err: 'notFound' });
                                        return;
                                    }

                                    params.max_timestamp = res.data[res.data.length - 1].created_time;
                                    res.geo = params;
                                    defer.resolve(res);
                                },
                                function(err) {
                                    defer.reject(err);
                                });
                        });
                    }

                    function getGeoPics() {
                        geolocation.getLocation().then(
                            function(data) {
                                getPics({ lat: data.coords.latitude, lng: data.coords.longitude, distance: 5000 });
                            },
                            function() {
                                defer.resolve({ err: 'setYourLocation' });
                            }
                        );
                    }

                    $stateParams.type === 'near' ?
                        getGeoPics() :
                        getPics({
                            lat: $stateParams.lat,
                            lng: $stateParams.lng,
                            distance: $stateParams.dist
                        });

                    return defer.promise;
                }
            },
            template: '<div insta-feed-list items="items" geo="geo" type="geo" err="{{ err }}"></div>',
            controller: 'feedCtrl',
            data: { pageTitle: 'Поиск по тегам' }
        });
})
.controller('feedCtrl', function($scope, $state, feed, $stateParams) {
    if (feed.err) {
        $scope.err = feed.err;
        return;
    }

    $scope.items = feed.data;
    $scope.pager = feed.pagination;
    $scope.geo = feed.geo;
    $scope.type = $stateParams.type;
    $scope.tag = $stateParams.tag;
});
