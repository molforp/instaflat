angular.module('pages.users', ['ui.router', 'services.instagram'])
.config(function($stateProvider) {
    $stateProvider
        .state('users', {
            url: '/users/:name',
            resolve: {
                users: function($q, $stateParams, $timeout, $state, instagramApiFactory) {
                    var name = $stateParams.name,
                        defer = $q.defer();

                    function getUsers(name) {
                        $timeout(function() {
                            instagramApiFactory.getUsers(name).then(
                                function(res) {
                                    defer.resolve(res);
                                },
                                function(err) {
                                    defer.reject(err);
                                });
                        });
                    }

                    name ? getUsers(name) : defer.resolve();

                    return defer.promise;
                }
            },
            controller: 'usersCtrl',
            template: '<div insta-people-search name="{{ name }}"></div>' +
                '<div class="user-list user-list_type_users"><div class="user-list__inner">' +
                '<div class="error-text" ng-if="err" translate="{{ err }}"></div>' +
                '<div insta-user user="user" ng-repeat="user in users track by $index"></div>' +
                '<div class="user-list__fade" ng-class="{ \'user-list__fade_visible\': feedIsLoading }">' +
                '</div></div></div>',
            data: { pageTitle: 'Люди' }
        });
})
.controller('usersCtrl', function($scope, $state, users, $stateParams) {
    $scope.users = users && users.data;
    $scope.name = decodeURIComponent($stateParams.name);
});
