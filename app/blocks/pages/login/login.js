angular.module('pages.login', ['ui.router'])
.config(function($stateProvider) {
    $stateProvider
        .state('login', {
            url: '/login',
            template: '<div insta-login ng-if="!isAuth"></div>',
            controller: 'loginCtrl',
            data: { pageTitle: 'Вход' }
        });
})
.controller('loginCtrl', function($scope, $state) {
    $scope.isAuth && $state.go('feed', { type: 'feed' });
});
