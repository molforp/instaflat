angular.module('pages.user', [
    'ui.router',
    'services.instagram',
    'pasvaz.bindonce'
])
.config(function($stateProvider) {
    $stateProvider
        .state('user', {
            abstract: true,
            url: '/user/:id',
            resolve: {
                user: function($q, $stateParams, instagramApiFactory, instagramUser) {
                    var id = $stateParams.id || 'self',
                        isItMe,
                        defer = $q.defer();

                    instagramUser.getId().then(function(uid) {
                        isItMe = id === 'self' || id === uid;
                        // если это я, то берем только инфу
                        // иначе, сначала узнаем приватный ли пользователь и фоловим ли мы его уже
                        isItMe ? getUserInfo() : getUserStatus();
                    });

                    function getUserInfo(isFollow) {
                        instagramApiFactory.getUser(id).then(function(res) {
                            _.assign(res.data, { isFollow: isFollow, isItMe: isItMe });

                            defer.resolve(res.data);
                        });
                    }

                    function getUserStatus() {
                        instagramApiFactory.isFollowed(id).then(function(res) {
                            var info = res.data,
                                isFollow = info.outgoing_status === 'follows',
                                isPrivat = info.target_user_is_private;

                            (isPrivat && !isFollow) ? defer.resolve({ isPrivat: true }) : getUserInfo(isFollow);
                        });
                    }



                    return defer.promise;
                }
            },
            template: '<div insta-profile user="user"></div><div ui-view ng-if="!user.isPrivat"></div>',
            controller: 'userCtrl',
            data: { pageTitle: 'Профиль' }
        })
        .state('user.feed', {
            url: '',
            data: { active: 'feed' },
            resolve: {
                feed: function($q, $timeout, instagramApiFactory, user) {
                    var defer = $q.defer();

                    if (user.isPrivat) return defer.reject();

                    $timeout(function() {
                        instagramApiFactory.getUserPics(user.id).then(
                            function(res) {
                                defer.resolve(res);
                            },
                            function(err) {
                                defer.reject(err);
                            });
                    });

                    return defer.promise;
                }
            },
            controller: function($scope, feed) {
                $scope.items = feed.data;
                $scope.pager = feed.pagination;
            },
            template: '<div insta-feed-list uid="{{ uid }}" items="items" pager="pager"></div>'
        })
        .state('user.followers', {
            url: '/followers',
            data: { active: 'followers' },
            resolve: {
                users: function($q, instagramApiFactory, user) {
                    var defer = $q.defer();

                    if (user.isPrivat) return defer.reject();

                    instagramApiFactory.getFollowers(user.id).then(
                        function(res) {
                            defer.resolve(res);
                        },
                        function(err) {
                            defer.reject(err);
                        });

                    return defer.promise;
                }
            },
            controller: function($scope, users) {
                $scope.users = users.data;
                $scope.pager = users.pagination;
            },
            template: '<div insta-user-list type="followers" uid="{{ uid }}" users="users" pager="pager"></div>'
        })
        .state('user.follows', {
            url: '/follows',
            data: { active: 'follows' },
            resolve: {
                users: function($q, instagramApiFactory, user) {
                    var defer = $q.defer();

                    if (user.isPrivat) return defer.reject();

                    instagramApiFactory.getFollows(user.id).then(
                        function(res) {
                            defer.resolve(res);
                        },
                        function(err) {
                            defer.reject(err);
                        });

                    return defer.promise;
                }
            },
            controller: function($scope, users) {
                $scope.users = users.data;
                $scope.pager = users.pagination;
            },
            template: '<div insta-user-list type="follows" uid="{{ uid }}" users="users" pager="pager"></div>'
        });
})
.controller('userCtrl', function($scope, $state, user) {
    $scope.user = user;

    if (user.isPrivat) return;

    $scope.uid = user.id;
});
