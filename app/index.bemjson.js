module.exports.bemjson = {
    block: 'html',
    content:  [
        { block: 'head' },
        {
            block: 'page',
            tag: 'body',
            attrs: { bindonce: '', 'bo-class': '{ ua_os_mac: isMac }' },
            content: [
                '<!--[if gt IE 8]><!-->',
                {
                    elem: 'inner',
                    content: [
                        // {
                        //     block: 'adv',
                        //     mods: { pos: 'top' },
                        //     content: {
                        //         block: 'link',
                        //         tag: 'a',
                        //         attrs: {
                        //             href: '//vk.com/moonhelp#insta',
                        //             target: 'blank',
                        //             'ng-click': 'countAdvClick()'
                        //         },
                        //         content: {
                        //             block: 'banner',
                        //             mods: { type: 'moon' }
                        //         }
                        //     }
                        // },
                        {
                            block: 'header',
                            tag: 'header',
                            attrs: {  'ng-class': '{ header_visible: isAuth }' },
                            content: [
                                {
                                    elem: 'left',
                                    content: {
                                        block: 'link',
                                        tag: 'a',
                                        attrs: { 'ui-sref': 'feed({ type: "feed" })' },
                                        content: {
                                            block: 'logo'
                                        }
                                    }
                                },
                                {
                                    elem: 'mid',
                                    content: {
                                        block: 'menu',
                                        mods: { type: 'header' }
                                    }
                                },
                                {
                                    elem: 'right',
                                    content: [
                                        {
                                            elem: 'invite',
                                            mix: { block: 'hint' },
                                            attrs: {
                                                'ng-click': 'inviteUser()',
                                                'data-name': '{{ "inviteFriends" | translate }}'
                                            },
                                            content: {
                                                block: 'icon',
                                                mods: { type: 'plus' }
                                            }
                                        },
                                        {
                                            elem: 'search',
                                            content: {
                                                attrs: { 'insta-header-search': '' }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'content',
                            attrs: { 'ui-view': '' }
                        },
                        {
                            tag: 'script',
                            attrs: { src: '//ad.mail.ru/static/vkadman.min.js' }
                        },
                        {
                            tag: 'script',
                            attrs: { src: '//js.appscentrum.com/scr/preroll.js' }
                        },
                        '<div id="vk_ads_56093"></div>',
                        {
                            block: 'error-alert',
                            attrs: {
                                'ng-class': '{ "error-alert_visible": isError }',
                                translate: '{{ errorAlert }}'
                            }
                        }
                    ]
                },
                '<!--inject:js--><!--endinject-->'
            ]
        }
    ]
};

module.exports.bh = function(bh) {
    bh.match('html', function(ctx, json) {
        return [
            '<!doctype html>',
            {
                tag: 'html',
                attrs: { 'ng-app': 'instaflat', 'ng-controller': 'appCtrl' },
                content: json.content
            }
        ];
    });

    bh.match('head', function() {
        return {
            tag: 'head',
            content: [
                {
                    tag: 'meta',
                    attrs: { charset: 'utf-8' }
                },
                {
                    tag: 'meta',
                    attrs: { name: 'description', content: '' }
                },
                {
                    tag: 'meta',
                    attrs: { name: 'keywords', content: '' }
                },
                {
                    tag: 'meta',
                    attrs: { name: 'viewport', content: 'width=device-width' }
                },
                {
                    tag: 'title',
                    content: 'Instaflat'
                },
                {
                    tag: 'link',
                    attrs: {
                        href: '//fonts.googleapis.com/css?family=Open+Sans:400,600&subset=latin,cyrillic',
                        rel: 'stylesheet'
                    }
                },
                // {
                //     tag: 'script',
                //     attrs: { src: '//www.parsecdn.com/js/parse-1.2.19.min.js' }
                // },
                {
                    tag: 'script',
                    attrs: { src: '//vk.com/js/api/xd_connection.js?2' }
                },
                '<!--[if lt IE 9]><script>',
                    'var userLang = window.navigator.browserLanguage, langs = { ru: {}, en: {} };',
                    'langs.ru.uotdated = "Ваш браузер устарел."; langs.ru.upgrade = "Обновите Ваш браузер";',
                    'langs.en.uotdated = "You are using an outdated browser.";',
                    'langs.en.upgrade = "Upgrade your browser";',
                    'document.body.innerHTML = "<div style=\'font-size: 20px; padding: 30px; text-align: center;\'>"',
                    ' + langs[userLang].uotdated',
                    ' + " <a href=\'http://browsehappy.com/\'>" + langs[userLang].upgrade + "</a></div>"',
                '</script><![endif]-->',
                '<!--inject:css--><!--endinject-->'
            ]
        };
    });

    bh.match('menu_type_header', function(ctx) {
        var items = [
            { type: 'feed', icon: 'home' },
            { type: 'popular', icon: 'star' },
            { type: 'likes', icon: 'like' },
            { type: 'near', icon: 'location', sref: 'geo({ type: "near", lat: "", lng: "" })' },
            { type: 'users', sref: 'users' },
            { type: 'self', icon: 'profile', sref: 'user.feed({ id: "self" })' }
        ];

        ctx.content(items.map(function(item) {
            return {
                elem: 'item',
                tag: 'li',
                mods: { type: item.type },
                attrs: { 'ui-sref-active': 'menu__item_active_yes' },
                content: {
                    block: 'link',
                    tag: 'a',
                    attrs: {
                        'ui-sref': item.sref ? item.sref : 'feed({ type: "' + item.type + '" })',
                        'ui-sref-opts': '{ reload: true, notify: true }',
                        'ng-click': 'alert(1)'
                    },
                    content: [
                        {
                            block: 'icon',
                            mods: { type: item.icon ? item.icon : item.type }
                        },
                        {
                            block: 'menu',
                            elem: 'item-text',
                            attrs: { translate: item.type }
                        }
                    ]
                }
            };
        }), true);
    });
};
