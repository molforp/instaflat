angular.module('instaflat', [
    // app config with constants (dynamic)
    'app.config',

    // cached templates (dynamic)
    'app.tpl',

    // pages
    'pages.feed',
    'pages.user',
    'pages.users',
    'pages.login',

    // components
    'components.login',
    'components.feedList',
    'components.userList',
    'components.box',
    'components.user',
    'components.profile',
    'components.popup',
    'components.select',
    'components.langSwitcher',
    'components.headerSearch',
    'components.peopleSearch',
    'components.i-utils',

    // services
    'services.i18n',
    'services.instagram',
    'services.popup',
    'services.parse',
    'services.metrika',
    'services.errorHandler',
    'services.vk',

    // libs
    'ui.router',
    'ngSanitize',
    'parse-angular',
    // 'parse-angular.enhance',
    'pasvaz.bindonce',
    'ngProgressLite',
    'geolocation'
])
.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/feed/feed');
    // $locationProvider.hashPrefix('!');

    // for using url without hash
    // $locationProvider.html5Mode(true);
})
.controller('appCtrl', function($q, $scope, $rootScope, $state, $location, $timeout, $window, $document, i18n,
            instagramAuthFactory, ngProgressLite, popupService) {
    $rootScope.instaToken = instagramAuthFactory.getToken();

    $scope.isMac = $window.navigator.userAgent.search(/Mac OS X/i) !== -1;
    $scope.pageTitle = 'Instaflat';
    $scope.logout = instagramAuthFactory.logout;

    $scope.$on('$stateChangeStart', function(e, toState) {
        $scope.isAuth = instagramAuthFactory.isAuth();
        $scope.feedIsLoading = true;

        toState.name === 'login' || ngProgressLite.start();
        popupService.isOpened && popupService.close();

        if (!$scope.isAuth && toState.name !== 'login') {
            ngProgressLite.done();
            $state.go('login');
            e.preventDefault();
        }
    });

    $scope.$on('$stateChangeSuccess', function(e, toState, toParams) {
        var paramsData = toState.data || {};
        $scope.activeState = toParams.type || toParams.id;
        $scope.feedIsLoading = false;

        if (paramsData.pageTitleKey) {
            $scope.pageTitleKey = paramsData.pageTitleKey;
        }

        ngProgressLite.done();

        $window.scrollTo(0, 0);
    });

    $scope.$on('$stateChangeError', function() {
        ngProgressLite.done();
    });

    $scope.inviteUser = function() {
        Parse.Analytics.track('show_invite_box');
        VK.callMethod('showInviteBox');
    };

    $scope.countAdvClick = function() {
        Parse.Analytics.track('top_adv_click');
    };
});
