angular.module('app.config', []).constant('config', {
    'tplPath': 'dist/tpl'
});

angular.module('app.tpl', []);

describe('appCtrl', function() {
    var appCtrl, $location, $scope;

    beforeEach(module('ng-shoot'));

    beforeEach(inject(function($controller, _$location_, $rootScope) {
        $location = _$location_;
        $scope = $rootScope.$new();
        appCtrl = $controller('appCtrl', { $location: $location, $scope: $scope });
    }));

    it('should pass a dummy test', inject(function() {
        expect(appCtrl).toBeTruthy();
    }));
});
