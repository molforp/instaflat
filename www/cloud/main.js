var Image = require("parse-image");
// var restler = require("cloud/node_modules/restler/lib/restler.js");
var API_URL = 'https://api.instagram.com/v1/';
// console.log(restler)

function onSuccess(httpRes, res) {
    httpRes.data.meta.code == 200 ? res.success() : res.error();
}

function onError(errRes, res) {
    var meta = (errRes.data || {}).meta || {},
        errText = meta.error_type ? meta.error_type + ': ' + meta.error_message : 'INSTAGRAM API ERROR';

    res.error(meta.error_type);
}

// like, unlike photo
Parse.Cloud.define('likeUnlike', function(req, res) {
    var params = req.params,
        type = params.type,
        method = type === 'like' ? 'POST' : type === 'unlike' ? 'DELETE' : 'POST';

    Parse.Cloud.httpRequest({
        method: method,
        url: API_URL + 'media/' + params.id + '/likes?access_token=' + params.token,
        success: function(httpRes) {
            onSuccess(httpRes, res);
        },
        error: function(errRes) {
            onError(errRes, res);
        }
    });
});

// follow, unfollow user
Parse.Cloud.define('followUnfollow', function(req, res) {
    var params = req.params;

    Parse.Cloud.httpRequest({
        method: 'POST',
        url: API_URL + 'users/' + params.id + '/relationship',
        body: {
            access_token: params.token,
            action: params.action
        },
        success: function(httpRes) {
            onSuccess(httpRes, res);
        },
        error: function(errRes) {
            onError(errRes, res);
        }
    });
});

function savePhoto(serverUrl, buffer) {
    console.log(Parse.Cloud.httpRequest({
        method: 'POST',
        url: serverUrl,
        body: { photo: buffer },
        headers: {
            'Content-Length': '72'
        },
        success: function(httpRes) {
            // httpRes.success(httpRes)
            console.log(httpRes)
        },
        error: function(errRes) {
            res.error(errRes)
        }
    }));
}

Parse.Cloud.define('savePhotoToWall', function(req, res) {
    var params = req.params;

    Parse.Cloud.httpRequest({
        url: params.photoUrl,
    })
    .then(function(image) {
        savePhoto(params.serverUrl, image.buffer);
        // Save the image into a new file.
        // var base64 = response.buffer.toString("base64"),
        //     image = new Parse.File("test.jpg", { base64: base64 });

        // return image.save();
        // var cropped = new Parse.File("thumbnail.jpg", { base64: base64 });
    });
});
