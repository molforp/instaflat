module.exports = {
    options: {
        node: true,
        bitwise: true,
        undef: true,
        unused: true,
        eqeqeq: true,
        forin: true,
        noarg: true,
        indent: 4,
        expr: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        noempty: true,
        nonew: true,
        trailing: true,
        quotmark: 'single'
    },
    groups: {
        client: {
            options: {
                browser: true,
                devel: false,
                debug: false,
                expr: true,
                predef: ['angular', '_', 'Parse', 'VK']
            },
            includes: [
                'app/**/*.js'
            ],
            excludes: [
                'app/**/*.spec.js'
            ]
        },

        test: {
            options: {
                predef: ['angular', 'describe', 'beforeEach', 'afterEach', 'inject', 'it', 'expect', '_']
            },
            includes: [
                'app/**/*.spec.js'
            ]
        }
    }
};
