var app = 'app',
    libs = app + '/libs',
    dist = 'www/public';

module.exports = {
    app: app,
    dist: dist,
    distTpl: dist + '/tpl',
    tmp: '.tmp',

    /**
     * App files
     */
    appFiles: {
        js: [
            app + '/*.js',
            app + '/blocks/**/*.js',
        ],

        excludedJs: [
            '!' + app + '/**/*.spec.js',
            '!' + app + '/**/*.bemjson.js'
        ],

        unit: [
            app + '/*.spec.js',
            app + '/blocks/**/*.spec.js',
        ],

        css: [
            app + '/*.css',
            app + '/blocks/**/*.css'
        ],

        images: app + '/**/i/*.{jpg,jpeg,png,ico,gif}',

        fonts: app + '/**/fonts/*.{ttf,eot,woff,svg}'
    },

    /**
     * Vendor files
     */
    libsFiles: {
        js: [
            libs + '/angular/angular.js',
            libs + '/angular-ui-router/release/angular-ui-router.js',
            libs + '/angular-bindonce/bindonce.js',
            libs + '/angular-cookies/angular-cookies.js',
            // libs + '/angular-animate/angular-animate.js',
            libs + '/angular-sanitize/angular-sanitize.js',
            libs + '/ngprogress-lite/ngprogress-lite.js',
            libs + '/angular-yandex-map/ya-map-2.1.min.js',
            libs + '/angular-cookie/angular-cookie.js',
            libs + '/angular-translate/angular-translate.js',
            libs + '/angular-translate-storage-cookie/angular-translate-storage-cookie.js',
            libs + '/angular-translate-storage-local/angular-translate-storage-local.js',
            libs + '/lodash/dist/lodash.js',
            libs + '/parse-angular-patch/dist/parse-angular.js',
            libs + '/angularjs-geolocation/src/geolocation.js'
        ],

        css: [
            libs + '/normalize-css/normalize.css',
            libs + '/ngprogress-lite/ngprogress-lite.css'
        ]
    },

    /**
     * For testing only
     */
    testFiles: {
        js: [
            libs + '/angular-mocks/angular-mocks.js'
        ]
    }
};
