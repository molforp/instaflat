var gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    path = require('path'),
    map = require('map-stream'),
    paths = require('./paths.conf.js'),
    config = require('./tech.conf.js'),
    isJade = config.tech === 'jade',
    isBEMJSON = config.tech === 'bemjson',
    tplExt = isJade ? 'jade' : isBEMJSON ? 'bemjson.js' : 'html',
    ENV,
    IS_PROD,
    IS_PROD_SERV;

    console.log(tplExt)

setEnv();

/**
 * Build info to screen
 */
gulp.task('info', function() {
    var glog = $.util.log,
        colors = $.util.colors;

    glog(colors.green('-------------------------'));
    glog(colors.green('==> Rebuilding...'));
    glog(colors.green('==> Env:', colors.yellow(ENV)));
    glog(colors.green('-------------------------'));
});

/**
 * Clean 'dist' and '.tmp' directories before start
 */
gulp.task('clean', function() {
    return gulp.src([paths.dist, paths.tmp], { read: false })
        .pipe($.rimraf({ force: true }));
});

/**
 * Dynamic generation of angular 'app.congif' module with constants
 * from '.config/<env>.conf.json'
 */
gulp.task('config', function() {
    var env = IS_PROD ? 'prod' : ENV;

    gulp.src('.config/' + env + '.conf.json')
        .pipe($.ngConstant({ name: 'app.config' }))
        .pipe(gulp.dest(paths.tmp));
});

/**
 * Lint your js files
 */
gulp.task('lint', function() {
    return gulp.src(paths.appFiles.js)
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe(jshintErrorReporter())
        .pipe($.jscs());
});

/**
 * Transfer images into 'dist/i' folder
 */
gulp.task('images', function() {
    return gulp.src(paths.appFiles.images)
        .pipe($.flatten())
        .pipe(gulp.dest(paths.dist + '/i'));
});

/**
 * Transfer favicon into 'dist' folder
 */
gulp.task('ico', function() {
    return gulp.src(paths.app + '/*.ico')
        .pipe(gulp.dest(paths.dist));
});

/**
 * Minify lang json files and transfer them into 'dist/l10n' folder
 */
gulp.task('l10n', function() {
    gulp.src(paths.app + '/l10n/*.json')
        .pipe($.jsonminify())
        .pipe(gulp.dest(paths.dist + '/l10n'));
});

/**
 * Transfer fonts to 'dist/font' folder
 */
gulp.task('fonts', function() {
    return gulp.src(paths.appFiles.fonts)
        .pipe($.flatten())
        .pipe(gulp.dest(paths.dist + '/fonts'));
});

/**
 * Concat, minimize all css files and append content hash to filename
 */
gulp.task('css', function() {
    return gulp.src(paths.libsFiles.css.concat(paths.appFiles.css))
        .pipe($.concat('app.css'))
        .pipe($.minifyCss())
        .pipe($.rev())
        .pipe(gulp.dest(paths.dist));
});

/**
 * Minify HTML templates and transfer into the 'dist/tpl'
 */
gulp.task('pre-tpl', function() {
    return gulp.src(paths.app + '/blocks/**/*.' + tplExt)
        .pipe($.if(isJade, $.jade({ doctype: 'html' })))
        .pipe($.if(isBEMJSON, $.bemjson()))
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true,
            comments: true
        }))
        .pipe($.flatten())
        .pipe(gulp.dest(paths.distTpl));
});

/**
 * Preload HTML templates from 'dist/tpl' into the $templateCache
 */
gulp.task('tpl', ['pre-tpl'], function() {
    var distTplPath = IS_PROD ? 'tpl/' : paths.dist + '/tpl/';

    return gulp.src(paths.distTpl + '/*.html')
        .pipe($.angularTemplatecache('tpl.js', { module: 'app.tpl', root: distTplPath, standalone: true }))
        .pipe(gulp.dest(paths.tmp));
});

/**
 * Concat, minify all js files (templates file also) and append content hash to filename
 */
gulp.task('js', ['config', 'tpl'], function() {
    return gulp.src(paths.libsFiles.js.concat(
            paths.appFiles.js,
            paths.appFiles.excludedJs,
            paths.tmp + '/*.js'))
        .pipe($.concat('app.js'))
        .pipe($.ngAnnotate())
        .pipe($.uglify())
        .pipe($.rev())
        .pipe(gulp.dest(paths.dist));
});

/**
 * Unit tests
 */
gulp.task('test', function() {
    return gulp.src(paths.libsFiles.js.concat(paths.testFiles.js, paths.appFiles.js, paths.appFiles.unit))
        .pipe($.karma({
            configFile: 'test/karma.conf.js',
            action: 'run'
        }))
        .on('error', function(err) {
            // Make sure failed tests cause gulp to exit non-zero
            throw err;
        });
});

/**
 * Task for development mode. Inject all js and css files to index.html
 */
gulp.task('dev', ['config', 'tpl'], function() {
    return gulp.src(paths.app + '/index.' + tplExt)
        .pipe($.if(isJade, $.jade({ pretty: true, doctype: 'html' })))
        .pipe($.if(isBEMJSON, $.bemjson()))
        .pipe($.inject(gulp.src(paths.libsFiles.js.concat(
                paths.appFiles.js,
                paths.appFiles.excludedJs,
                paths.tmp + '/*.js',
                paths.libsFiles.css,
                paths.appFiles.css
            ),
            { read: false }),
            {
                starttag: '<!--inject:{{ext}}-->',
                endtag: '<!--endinject-->',
                addRootSlash: false,
                addPrefix: '../..'
            }))
        .pipe(gulp.dest(paths.dist))
        .pipe($.livereload());
});

/**
 * Task for production mode. Inject all js and css files to index.html
 */
gulp.task('prod', ['js', 'css', 'images', 'l10n', 'ico', 'fonts'], function() {
    IS_PROD_SERV && startServer();

    return gulp.src(paths.app + '/index.' + tplExt)
        .pipe($.if(isJade, $.jade({ doctype: 'html' })))
        .pipe($.if(isBEMJSON, $.bemjson()))
        .pipe($.inject(gulp.src(paths.dist + '/*.{js,css}', { read: false }), {
            starttag: '<!--inject:{{ext}}-->',
            endtag: '<!--endinject-->',
            addRootSlash: false,
            ignorePath: IS_PROD_SERV ? '' : paths.dist
        }))
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true,
            conditionals: true,
            comments: true
        }))
        .pipe(gulp.dest(paths.dist));
});

/**
 * Watch for changes
 */
gulp.task('watch', function() {
    startServer();

    $.watch({ glob: paths.appFiles.js.concat(paths.appFiles.css), emitOnGlob: false, silent: true }, function(files) {
        return files.pipe($.livereload());
    });

    $.watch({ glob: paths.app + '/**/*.{html,jade,bemjson.js}', emitOnGlob: false, silent: true }, function() {
        gulp.start('dev');
    });
});

/**
 * Default task
 */
gulp.task('default', ['info', 'clean'], function() {
    (IS_PROD || IS_PROD_SERV) ? gulp.start('prod') : gulp.start('dev', 'watch');
});

gulp.task('stage', ['info', 'clean'], function() {
    ENV = 'stage';
    gulp.start('prod');
});

/**
 * Set environment
 */
function setEnv() {
    var env = $.util.env;

    if (env.p || env.prod) {
        ENV = 'production';

        (env.s || env.serv) ? (IS_PROD_SERV = true) : (IS_PROD = true);
        return;
    }

    ENV = 'dev';
}

function jshintErrorReporter() {
    return map(function(file, cb) {
        file.jshint.success || process.exit(1);
        cb(null, file);
    });
}

/**
 * Start local server
 */
function startServer() {
    var express = require('express'),
        app = express(),
        exPort = 3000;

    app.use(express.static(process.cwd()));

    IS_PROD_SERV || app.use(require('connect-livereload')({ port: 35729 }));

    app.listen(exPort);

    app.get('*', function(req, res) {
        var getPath = req.url,
            staticPath = IS_PROD_SERV ? 'dist' : 'app',
            distPath = path.extname(getPath) ? staticPath + getPath : 'www/public/index.html';

            console.log(process.cwd() + '/' + distPath)

        res.sendfile(process.cwd() + '/' + distPath);
    });

    $.util.log('Express server listening on:', $.util.colors.magenta(exPort));
}
